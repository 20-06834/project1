import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  
  {
    path: '',
    redirectTo: 'splash',
    pathMatch: 'full'
  },
  {
    path: 'splash',
    loadChildren: () => import('./splash/splash.module').then( m => m.SplashPageModule)
  },
  {
    path: 'home',
    loadChildren: () => import('./home/home.module').then( m => m.HomePageModule)
  },
  {
    path: 'button1',
    loadChildren: () => import('./button1/button1.module').then( m => m.Button1PageModule)
  },
  {
    path: 'button2',
    loadChildren: () => import('./button2/button2.module').then( m => m.Button2PageModule)
  },
  {
    path: 'button3',
    loadChildren: () => import('./button3/button3.module').then( m => m.Button3PageModule)
  },
  {
    path: 'b1.page1',
    loadChildren: () => import('./b1.page1/b1.page1.module').then( m => m.B1Page1PageModule)
  },
  {
    path: 'b1.page2',
    loadChildren: () => import('./b1.page2/b1.page2.module').then( m => m.B1Page2PageModule)
  },
  {
    path: 'b1.page3',
    loadChildren: () => import('./b1.page3/b1.page3.module').then( m => m.B1Page3PageModule)
  },
  {
    path: 'b2.page1',
    loadChildren: () => import('./b2.page1/b2.page1.module').then( m => m.B2Page1PageModule)
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }

