import { Component } from '@angular/core';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-button2',
  templateUrl: './button2.page.html',
  styleUrls: ['./button2.page.scss'],
})
export class Button2Page {
  constructor(private navCtrl: NavController) {}

  onItemClick(item: string) {
    // Handle item click event here
    console.log(`Clicked: ${item}`);

    // Navigate to the desired page based on the clicked item
    if (item === 'Item 1') {
      this.navCtrl.navigateForward('b2.page1');
    }
    // Add similar navigation logic for other items if needed
  }
}
